package com.lstack.app1

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class Controller(val feignClient: FeignClient) {
    @GetMapping("/test1/get")
    fun test1Get(): String {
        return "test1/get"
    }

    @PostMapping("/test1/post")
    fun test1Post(@RequestBody map: MutableMap<String, String>): Any {
        map["app"] = "app1"
        return map
    }

    @PostMapping("/to/test2/post")
    fun test2Post(@RequestBody map: MutableMap<String, String>): Any {
        return feignClient.test2Post(map)
    }
}