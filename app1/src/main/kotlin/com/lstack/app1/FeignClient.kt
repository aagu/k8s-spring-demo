package com.lstack.app1

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping

@FeignClient(value = "app2")
@RequestMapping("/test2")
interface FeignClient {
    @PostMapping(value = ["/post"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun test2Post(@RequestBody map: MutableMap<String, String>): Any
}