package com.lstack.app2

import com.lstack.app2.entity.User
import com.lstack.app2.repo.UserRepo
import org.springframework.web.bind.annotation.*

@RestController
class Controller(var repo: UserRepo) {
    @GetMapping("/test2/get")
    fun test2Get(): Any {
        return "test2/get"
    }

    @PostMapping("/test2/post")
    fun test2Post(@RequestBody map: MutableMap<String, String>): Any {
        map["app"] = "app2"
        return map
    }

    @PostMapping("/db/install")
    fun installTable(): Any {
        repo.installTable()
        val map = HashMap<String, String>()
        map["status"] = "ok"
        return map
    }

    @GetMapping("/db/users")
    fun listUser(): Any {
        return repo.getAll()
    }

    @GetMapping("/db/user/{id}")
    fun getUser(@PathVariable id: Long): Any {
        return repo.getById(id)
    }

    @PostMapping("/db/users")
    fun addUser(@RequestBody user: User): Any {
        repo.add(user)
        return user
    }
}