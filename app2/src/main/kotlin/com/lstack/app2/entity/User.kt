package com.lstack.app2.entity

data class User(var id: Long, var name: String, var age: Int)
