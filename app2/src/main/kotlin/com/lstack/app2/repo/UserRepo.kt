package com.lstack.app2.repo

import com.lstack.app2.entity.User
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Select
import org.apache.ibatis.annotations.Update
import org.springframework.stereotype.Repository

@Repository
@Mapper
interface UserRepo {
    @Select("select id, name, age from user")
    fun getAll(): List<User>

    @Select("select id, name, age from user where id=#{id}")
    fun getById(id: Long): User

    @Insert("insert into user(name,age) values(#{name}, #{age})")
    fun add(user: User)

    @Update("create table user(id int AUTO_INCREMENT, name varchar(24), age int, PRIMARY KEY (id))")
    fun installTable()
}